//
//  FeedServiceInterface.h
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FeedModel;

@protocol FeedServiceInterface <NSObject>

- (void)getModelsWithSuccess:(void(^)(NSArray <FeedModel *> *models))success failure:(void(^)(NSError *error))failure;
- (NSArray *)getModelsFromDataStore;
@end
