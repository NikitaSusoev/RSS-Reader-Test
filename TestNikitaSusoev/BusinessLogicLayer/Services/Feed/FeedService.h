//
//  FeedService.h
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeedServiceInterface.h"

@class NetworkManager,RealmDataStore;
@protocol FeedResponseMapperInterface;

@interface FeedService : NSObject <FeedServiceInterface>

- (instancetype)initWithNetworkManager:(NetworkManager *)manager
                        responseMapper:(id <FeedResponseMapperInterface>)mapper
                             dataStore:(RealmDataStore *)dataStore;

@end
