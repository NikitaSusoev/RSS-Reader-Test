//
//  FeedService.m
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import "FeedService.h"
#import "NetworkManager.h"
#import "FeedResponseMapperInterface.h"
#import "RealmDataStore.h"
#import <Bolts.h>

static NSString * const kLentaURL = @"https://lenta.ru/rss";
static NSString * const kGazetaURL = @"https://www.gazeta.ru/export/rss/lenta.xml";

@interface FeedService() 

@property (strong, nonatomic) NetworkManager *manager;
@property (strong, nonatomic) id <FeedResponseMapperInterface> mapper;
@property (strong, nonatomic) RealmDataStore *dataStore;
@property (strong, nonatomic) NSMutableArray <FeedModel *> *models ;

@end

@implementation FeedService

- (instancetype)initWithNetworkManager:(NetworkManager *)manager
                        responseMapper:(id <FeedResponseMapperInterface>)mapper
                             dataStore:(RealmDataStore *)dataStore
{
    self = [super init];
    if (self) {
        _manager = manager;
        _mapper = mapper;
        _dataStore = dataStore;
        _models = [NSMutableArray new];
    }
    
    return self;
}

- (void)getModelsWithSuccess:(void(^)(NSArray <FeedModel *> *models))success failure:(void(^)(NSError *error))failure {
    [self.models removeAllObjects];
        __weak typeof(self) weakSelf = self;
        [[self p_taskGetAllFeed] continueWithBlock:^id _Nullable(BFTask * _Nonnull t) {
            if (t.error) {
                failure(t.error);
            } else {
                [weakSelf.dataStore saveFeedModels:weakSelf.models];
                success(weakSelf.models);
            }
            return nil;
        }];
}

- (NSArray *)getModelsFromDataStore {
    return [self.dataStore getFeedModels];
}

- (BFTask *)p_taskGetAllFeed {
    NSArray *feeds = @[kLentaURL, kGazetaURL];
    NSMutableArray *tasks = [NSMutableArray array];
    for (NSString *URLString in feeds) {
        [tasks addObject:[self p_taskGetFeedWithURLString:URLString]];
    }
    return [BFTask taskForCompletionOfAllTasks:tasks];
}

- (BFTask *)p_taskGetFeedWithURLString:(NSString *)URLString {
    BFTaskCompletionSource *source = [BFTaskCompletionSource taskCompletionSource];
    __weak typeof(self) weakSelf = self;
    [self.manager getFeedWithURLString:URLString success:^(id responseObject) {
        [weakSelf.models addObjectsFromArray:[weakSelf.mapper mapWithResponse:responseObject]];
        [source setResult:responseObject];
    } failure:^(NSError *error) {
        [source setError:error];
    }];
    return source.task;
}

@end
