//
//  AppDelegate.h
//  TestNikitaSusoev
//
//  Created by Никита on 22.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

