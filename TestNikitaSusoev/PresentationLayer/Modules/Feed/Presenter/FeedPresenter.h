//
//  FeedPresenter.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeedViewOutput.h"
#import "FeedInteractorOutput.h"
#import "FeedModuleInput.h"

@protocol FeedViewInput;
@protocol FeedInteractorInput;
@protocol FeedRouterInput;
@protocol FeedModuleOutput;

@interface FeedPresenter : NSObject <FeedModuleInput, FeedInteractorOutput, FeedViewOutput>

@property (nonatomic, weak) id<FeedViewInput> view;
@property (nonatomic, strong) id<FeedInteractorInput> interactor;
@property (nonatomic, strong) id<FeedRouterInput> router;
@property (nonatomic, weak) id<FeedModuleOutput> moduleOutput;

@end
