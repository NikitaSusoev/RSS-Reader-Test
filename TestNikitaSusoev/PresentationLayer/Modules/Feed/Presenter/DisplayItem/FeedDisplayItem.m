//
//  FeedDisplayItem.m
//  TestNikitaSusoev
//
//  Created by Никита on 24.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import "FeedDisplayItem.h"
#import "FeedModel.h"

static NSString * const kFeedCellIdentifier = @"FeedCell";

@implementation FeedDisplayItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        _cellIdentifier = kFeedCellIdentifier;
        _isExpanded = NO;
    }
    return self;
}

+ (instancetype)itemFromFeedModel:(FeedModel *)model {
    FeedDisplayItem *item = [FeedDisplayItem new];
    item.pubDate = model.date;
    item.objectId = model.objectId;
    item.title = model.title;
    item.source = model.source;
    item.caption = model.caption;
    item.imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:model.imageURL]];
    return item;
}

@end
