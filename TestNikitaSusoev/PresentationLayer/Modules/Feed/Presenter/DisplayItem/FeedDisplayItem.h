//
//  FeedDisplayItem.h
//  TestNikitaSusoev
//
//  Created by Никита on 24.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FeedModel;

@interface FeedDisplayItem : NSObject

@property (strong, nonatomic) NSString *source;
@property (strong, nonatomic) NSDate *pubDate;
@property (strong, nonatomic) NSString *objectId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *caption;
@property (strong, nonatomic) NSData *imageData;
@property (assign, nonatomic) BOOL isExpanded;
@property (strong, nonatomic) NSString *cellIdentifier;

+ (instancetype)itemFromFeedModel:(FeedModel *)model;

@end
