//
//  FeedPresenter.m
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import "FeedPresenter.h"

#import "FeedViewInput.h"
#import "FeedInteractorInput.h"
#import "FeedRouterInput.h"
#import "FeedModuleOutput.h"
#import "FeedDisplayItem.h"
#import "NSArray+Extras.h"

static NSString * const kPubDate = @"pubDate";

@interface FeedPresenter()
@property (strong, nonatomic) NSMutableArray <FeedDisplayItem *> *items;
@end

@implementation FeedPresenter

- (instancetype)init
{
    self = [super init];
    if (self) {
        _items = [NSMutableArray new];
    }
    return self;
}

#pragma mark - FeedViewOutput

- (void)refreshWithCompleteion:(void(^)(void))completion {
    [self.interactor refreshWithCompleteion:completion];
}

- (void)didTriggerViewReadyEvent {
    [self.view startActivityIndicator];
    [self.interactor updateItems];
}

- (void)didTriggerViewWillAppearEvent {
}

-(void)showItems:(NSArray <FeedDisplayItem *> *)items {
    [self.view stopActivityIndicator];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:kPubDate ascending:NO];
    _items = [[items sortedArrayUsingDescriptors:@[descriptor]] mutableCopy];
    [self.view showDisplayItems:_items];
}

- (void)changeExpandStateItemByIndex:(NSInteger)index {
    FeedDisplayItem *item = self.items[index];
    item.isExpanded = item.isExpanded ? NO : YES;
    [self.view expandAtIndex:index withItems:self.items];
}

#pragma mark - FeedInteractorOutput


#pragma mark - FeedModuleInput


#pragma mark - Private


@end
