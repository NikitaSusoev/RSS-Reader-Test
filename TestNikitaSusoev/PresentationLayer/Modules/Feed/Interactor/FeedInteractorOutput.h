//
//  FeedInteractorOutput.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FeedDisplayItem;

@protocol FeedInteractorOutput <NSObject>

-(void)showItems:(NSArray<FeedDisplayItem *>*)items;

@end
