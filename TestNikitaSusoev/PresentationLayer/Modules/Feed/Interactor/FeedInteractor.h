//
//  FeedInteractor.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import "FeedInteractorInput.h"

@protocol FeedInteractorOutput, FeedServiceInterface;

@interface FeedInteractor : NSObject <FeedInteractorInput>

@property (nonatomic, weak) id<FeedInteractorOutput> output;

- (instancetype)initWithFeedService:(id <FeedServiceInterface>)feedService;

@end
