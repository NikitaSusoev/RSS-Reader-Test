//
//  FeedInteractorInput.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FeedModel;

@protocol FeedInteractorInput <NSObject>

- (void)updateItems;
- (void)refreshWithCompleteion:(void(^)(void))completion;

@end
