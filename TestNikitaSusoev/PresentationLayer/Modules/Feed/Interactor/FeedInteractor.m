//
//  FeedInteractor.m
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import "FeedInteractor.h"
#import "FeedInteractorOutput.h"
//#import "ASIHTTPRequest.h"
#import <AFNetworking.h>
#import "FeedDisplayItem.h"
#import "Ono.h"
#import "FeedServiceInterface.h"

@interface FeedInteractor ()

@property (strong, nonatomic) NSMutableArray *allEntries;
@property (strong, nonatomic) id <FeedServiceInterface> feedService;

@end

@implementation FeedInteractor

- (instancetype)initWithFeedService:(id <FeedServiceInterface>)feedService
{
    self = [super init];
    if (self) {
        _feedService = feedService;
        _allEntries = [NSMutableArray array];
    }
    return self;
}

#pragma mark - FeedInteractorInput

- (void)updateItems {
    NSArray *feedModels = [self.feedService getModelsFromDataStore];
    if ([feedModels count]) {
        [self p_showItemsWithModels:feedModels];
    } else {
        __weak typeof(self) weakSelf = self;
        [self.feedService getModelsWithSuccess:^(NSArray<FeedModel *> *models) {
            [weakSelf p_showItemsWithModels:models];
        } failure:^(NSError *error) {
            //TODO:handle error
        }];
    }
}

- (void)refreshWithCompleteion:(void(^)(void))completion {
    __weak typeof(self) weakSelf = self;
    [self.feedService getModelsWithSuccess:^(NSArray<FeedModel *> *models) {
        [weakSelf p_showItemsWithModels:models];
        completion();
    } failure:^(NSError *error) {
        //TODO:handle error
    }];
}

#pragma mark - Private

-(void)p_showItemsWithModels:(NSArray <FeedModel *> *)models {
    NSArray *items = [self p_displayItemsFromModels:models];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.output showItems:items];
    });
}

-(NSArray <FeedDisplayItem *> *)p_displayItemsFromModels:(NSArray <FeedModel *> *)models {
    NSMutableArray *items = [NSMutableArray new];
    for (FeedModel *model in models) {
        FeedDisplayItem *item = [FeedDisplayItem itemFromFeedModel:model];
        [items addObject:item];
    }
    return [items copy];
}

@end
