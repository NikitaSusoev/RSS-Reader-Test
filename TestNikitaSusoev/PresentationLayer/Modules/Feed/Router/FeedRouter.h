//
//  FeedRouter.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeedRouterInput.h"
@protocol FeedModuleInput;

@interface FeedRouter : NSObject<FeedRouterInput>

@property (weak, nonatomic) UIViewController *view;

@end
