//
//  FeedRouter.m
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import "FeedRouter.h"

@implementation FeedRouter

#pragma mark - FeedRouterInput

- (void)openModuleFromViewController:(UIViewController *)viewController {
    [viewController presentViewController:self.view animated:YES completion:nil];
}

@end
