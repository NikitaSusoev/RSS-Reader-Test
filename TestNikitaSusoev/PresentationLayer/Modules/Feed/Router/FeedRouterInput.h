//
//  FeedRouterInput.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeedRouterInput <NSObject>

- (void)openModuleFromViewController:(UIViewController *)viewController;

@end
