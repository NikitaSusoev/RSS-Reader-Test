//
//  FeedAssembly.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeedModuleInput.h"
#import "FeedRouterInput.h"

@protocol FeedModuleOutput;

@interface FeedAssembly : NSObject

+ (void)assembleModuleWithOutput:(id <FeedModuleOutput>)moduleOutput transitionBlock:(void(^)(id<FeedRouterInput> router))transitionBlock completion:(void(^)(id<FeedModuleInput> input))completion;

@end
