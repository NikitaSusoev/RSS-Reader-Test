//
//  FeedAssembly.m
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FeedAssembly.h"
#import "FeedViewController.h"
#import "FeedPresenter.h"
#import "FeedRouter.h"
#import "FeedInteractor.h"

#import "FeedService.h"
#import "NetworkManager.h"
#import "FeedResponseMapper.h"
#import "RealmDataStore.h"

static NSString *FeedViewControllerIdentifier = @"FeedViewControllerIdentifier";
static NSString *FeedViewControllerStoryboardName = @"FeedViewController";


@interface FeedAssembly ()

@property (strong, nonatomic) FeedViewController *view;
@property (strong, nonatomic) FeedPresenter *presenter;
@property (strong, nonatomic) FeedRouter *router;
@property (strong, nonatomic) FeedInteractor *interactor;

@end

@implementation FeedAssembly

#pragma mark - Public
+ (void)assembleModuleWithOutput:(id <FeedModuleOutput>)moduleOutput transitionBlock:(void(^)(id<FeedRouterInput> router))transitionBlock completion:(void(^)(id<FeedModuleInput> input))completion {
  [[FeedAssembly new] assembleModuleWithOutput:moduleOutput transitionBlock:transitionBlock completion:completion];
}

#pragma mark private instance method analog
- (void)assembleModuleWithOutput:(id <FeedModuleOutput>)moduleOutput transitionBlock:(void(^)(id<FeedRouterInput> router))transitionBlock completion:(void(^)(id<FeedModuleInput> input))completion {
  [self buildComponents];
  [self configureDependenciesWithModuleOutput:moduleOutput];

  [_view setupViewReadyBlock:^{
    [self addChildComponents];
      if (completion) {
        completion(_presenter);
      }
  }];

  if (transitionBlock) {
    transitionBlock(_router);
  }
}

#pragma mark - Child components
- (void)addChildComponents {
  // Present child submodules here
}

#pragma mark - Private
#pragma mark - Dependencies
- (void)buildComponents {
  _view = [self viewFeed];
  _presenter = [self presenterFeed];
  _router = [self routerFeed];
  _interactor = [self interactorFeed];
}
- (void)configureDependenciesWithModuleOutput:(id <FeedModuleOutput>)moduleOutput {
  _presenter.view = _view;
  _view.output = _presenter;

  _presenter.router = _router;
  _router.view = _view;

  _presenter.interactor = _interactor;
  _interactor.output = _presenter;

  _presenter.moduleOutput = moduleOutput;
}

#pragma mark - Factory methods
- (FeedViewController *)viewFeed {
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:FeedViewControllerStoryboardName
                                                         bundle:nil];
  FeedViewController *vc = [storyboard instantiateViewControllerWithIdentifier:FeedViewControllerIdentifier];
  return vc;
}
- (FeedPresenter *)presenterFeed {
  return [[FeedPresenter alloc] init];
}
- (FeedRouter *)routerFeed {
  return [[FeedRouter alloc] init];
}
- (FeedInteractor *)interactorFeed {
    NetworkManager *manager = [NetworkManager sharedManager];
    FeedResponseMapper *responseMapper = [FeedResponseMapper new];
    RealmDataStore *store = [RealmDataStore defaultStore];
    FeedService *service = [[FeedService alloc] initWithNetworkManager:manager
                                                        responseMapper:responseMapper
                                                             dataStore:store];
  return [[FeedInteractor alloc] initWithFeedService:service];
}

@end
