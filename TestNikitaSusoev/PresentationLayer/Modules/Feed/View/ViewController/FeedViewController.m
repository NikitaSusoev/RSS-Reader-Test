//
//  FeedViewController.m
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import "FeedViewController.h"
#import "FeedViewOutput.h"
#import "FeedCellInterface.h"
#import "FeedDisplayItem.h"
#import "FeedCell.h"

@interface FeedViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (copy, nonatomic) void (^viewReadyBlock)(void);
@property (strong, nonatomic) NSArray  *items;

@end

@implementation FeedViewController

- (void)setupViewReadyBlock:(void(^)(void))block {
  _viewReadyBlock = block;
}

#pragma mark - Lifecycle
- (void)viewDidLoad {
  [super viewDidLoad];
  UIView *view = self.view;
  if (view && _viewReadyBlock) {
    _viewReadyBlock();
    _viewReadyBlock = nil;
  }
    [self p_configureView];
    [self.output didTriggerViewReadyEvent];
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self.output didTriggerViewWillAppearEvent];
}

#pragma mark - FeedViewInput

- (void)showDisplayItems:(NSArray <FeedDisplayItem *> *)items {
    self.items = items;
    [self.tableView reloadData];
}

- (void)expandAtIndex:(NSInteger)index
            withItems:(NSArray <FeedDisplayItem *> *)items {
    self.items = items;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)startActivityIndicator {
    [self.activityIndicator startAnimating];
}

- (void)stopActivityIndicator {
    [self.activityIndicator stopAnimating];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.output changeExpandStateItemByIndex:indexPath.row];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeedDisplayItem *item = self.items[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:item.cellIdentifier forIndexPath:indexPath];
    if ([cell conformsToProtocol:@protocol(FeedCellInterface)]) {
        [(UITableViewCell <FeedCellInterface> *)cell  configureCellWithItem:item];
    }
    return cell;
}

#pragma mark - Private

- (void)p_configureView {
    NSString *className = NSStringFromClass([FeedCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:className bundle:[NSBundle mainBundle]] forCellReuseIdentifier:className];
    [self.activityIndicator setHidesWhenStopped:YES];
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    self.tableView.refreshControl = self.refreshControl;
    [self.refreshControl addTarget:self
                            action:@selector(p_refresh)
                  forControlEvents:UIControlEventValueChanged];
}

-(void)p_refresh {
    __weak typeof(self) weakSelf = self;
    [self.output refreshWithCompleteion:^{
        [weakSelf.refreshControl endRefreshing];
    }];
}

@end
