//
//  FeedViewController.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedViewInput.h"

@protocol FeedViewOutput;

@interface FeedViewController : UIViewController <FeedViewInput>

@property (nonatomic, strong) id<FeedViewOutput> output;

- (void)setupViewReadyBlock:(void(^)(void))block;

@end
