//
//  FeedViewOutput.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FeedViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;
- (void)didTriggerViewWillAppearEvent;
- (void)changeExpandStateItemByIndex:(NSInteger)index;
- (void)refreshWithCompleteion:(void(^)(void))completion;

@end
