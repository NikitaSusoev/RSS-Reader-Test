//
//  FeedCell.m
//  TestNikitaSusoev
//
//  Created by Никита on 24.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import "FeedCell.h"
#import "FeedCellInterface.h"
#import "FeedDisplayItem.h"

@interface FeedCell()<FeedCellInterface>
@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *source;
@property (weak, nonatomic) IBOutlet UILabel *caption;

@end

@implementation FeedCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.caption.text = nil;
    self.source.text = nil;
    self.title.text = nil;
    self.pictureImageView.image = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureCellWithItem:(FeedDisplayItem *)item {
    self.pictureImageView.image = [UIImage imageWithData:item.imageData];
    self.title.text = item.title;
    self.source.text = item.source;
    self.caption.text = item.isExpanded ? item.caption : nil;
}

@end
