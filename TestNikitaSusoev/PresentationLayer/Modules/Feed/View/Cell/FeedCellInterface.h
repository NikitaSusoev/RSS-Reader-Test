//
//  FeedCellInterface.h
//  TestNikitaSusoev
//
//  Created by Никита on 24.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FeedDisplayItem;

@protocol FeedCellInterface <NSObject>

- (void)configureCellWithItem:(FeedDisplayItem *)item;

@end
