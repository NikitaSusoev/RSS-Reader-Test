//
//  FeedViewInput.h
//  TestNikitaSusoev
//
//  Created by Никита on 23/02/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FeedDisplayItem;

@protocol FeedViewInput <NSObject>

- (void)showDisplayItems:(NSArray <FeedDisplayItem *> *)items;
- (void)expandAtIndex:(NSInteger)index
            withItems:(NSArray <FeedDisplayItem *> *)items;
- (void)startActivityIndicator;
- (void)stopActivityIndicator;

@end
