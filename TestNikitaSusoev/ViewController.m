//
//  ViewController.m
//  TestNikitaSusoev
//
//  Created by Никита on 22.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import "ViewController.h"
#import "FeedAssembly.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidAppear:(BOOL)animated {
    [FeedAssembly assembleModuleWithOutput:nil
                           transitionBlock:^(id<FeedRouterInput> router) {
                               [router openModuleFromViewController:self];
                           } completion:nil];
}

@end
