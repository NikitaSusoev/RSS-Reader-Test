//
//  RLMFeedEntity.m
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import "RLMFeedEntity.h"
#import "FeedModel.h"

@implementation RLMFeedEntity

+(instancetype)configureWithObject:(FeedModel *)obj {
    RLMFeedEntity *result = [self new];
    result.objectId = obj.objectId;
    result.title = obj.title;
    result.source = obj.source;
    result.caption = obj.caption;
    result.date = obj.date;
    result.imageURL = obj.imageURL;
    return result;
}

-(FeedModel *)configureObject {
    FeedModel *obj = [FeedModel new];
    obj.objectId = self.objectId;
    obj.title = self.title;
    obj.source = self.source;
    obj.caption = self.caption;
    obj.date = self.date;
    obj.imageURL = self.imageURL;
    return obj;
}

+ (NSString *)primaryKey {
    return @"objectId";
}

@end
