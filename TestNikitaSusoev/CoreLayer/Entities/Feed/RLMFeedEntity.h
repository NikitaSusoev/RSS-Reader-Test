//
//  RLMFeedEntity.h
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <Realm/Realm.h>

@class FeedModel;

@interface RLMFeedEntity : RLMObject

@property (strong, nonatomic) NSString *source;
@property (strong, nonatomic) NSString *objectId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *caption;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *imageURL;

+(instancetype)configureWithObject:(FeedModel *)obj;
-(FeedModel *)configureObject;

@end
