//
//  NetworkManager.m
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import "NetworkManager.h"
#import <AFNetworking.h>

@implementation NetworkManager

+ (instancetype) sharedManager
{
    static dispatch_once_t onceToken;
    static NetworkManager *manager;
    dispatch_once(&onceToken, ^{
        manager = [[NetworkManager alloc] init];
    });
    
    return manager;
}

- (void)getFeedWithURLString:(NSString *)URLString success:(void(^)(id responseObject))success failure:(void(^)(NSError *error))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer * requestSerializer = [AFHTTPRequestSerializer serializer];
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer = responseSerializer;
    manager.requestSerializer = requestSerializer;
        [manager GET:URLString
          parameters:nil
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure(error);
        }];
}


@end
