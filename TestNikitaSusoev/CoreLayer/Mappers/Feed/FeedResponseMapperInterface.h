//
//  FeedParserInterface.h
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FeedModel;

@protocol FeedResponseMapperInterface <NSObject>

- (NSArray <FeedModel *> *)mapWithResponse:(id)response;

@end
