//
//  FeedParser.m
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import "FeedResponseMapper.h"
#import <Ono.h>
#import "FeedModel.h"

@interface FeedResponseMapper() 

@end

@implementation FeedResponseMapper

- (NSArray <FeedModel *> *)mapWithResponse:(id)response {
    NSData * data = (NSData *)response;
    ONOXMLDocument *document = [ONOXMLDocument XMLDocumentWithData:data error:nil];
    NSMutableArray *feedModels = [NSMutableArray new];
    if (!document) {
        NSLog(@"Parsing is failed");
    } else {
        ONOXMLElement *element = [document.rootElement.children firstObject];
        NSString *source = [[element firstChildWithTag:@"link"] stringValue];
        
        NSArray *elements = [element childrenWithTag:@"item"];
        for (ONOXMLElement *tempElement in elements) {
            FeedModel *feed = [FeedModel new];
            feed.source = source;
            feed.objectId = [[tempElement firstChildWithTag:@"guid"] stringValue];
            feed.title = [[tempElement firstChildWithTag:@"title"] stringValue];
            feed.caption = [[tempElement firstChildWithTag:@"description"] stringValue];
            
            NSDateFormatter *formatter = [NSDateFormatter new];
            formatter.dateFormat = @"EEE, dd MMM yyyy HH:mm:ss ZZZZZ";
            formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
            feed.date = [formatter dateFromString:[[tempElement firstChildWithTag:@"pubDate"] stringValue]];
            
            feed.imageURL = [[tempElement firstChildWithTag:@"enclosure"] attributes][@"url"];
            [feedModels addObject:feed];
        }
    }
    return [feedModels copy];
}

@end
