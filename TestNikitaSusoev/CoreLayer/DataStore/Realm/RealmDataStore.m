//
//  RealmDataStore.m
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import "RealmDataStore.h"
#import <Realm.h>
#import "FeedModel.h"
#import "RLMFeedEntity.h"

@implementation RealmDataStore

+ (RealmDataStore *)defaultStore {
    
    static RealmDataStore *store;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        store = [[RealmDataStore alloc] init];
    });
    return store;
}

+ (void)setupMigrationIfNeeded {
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.schemaVersion = 3;
    [RLMRealmConfiguration setDefaultConfiguration:config];
}

- (void)saveFeedModels:(NSArray *)models {
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    BOOL inTransaction = [realm inWriteTransaction];
    
    if (!inTransaction) {
        [realm beginWriteTransaction];
    }
    
    for (FeedModel *model in models) {
        RLMFeedEntity *obj = [RLMFeedEntity configureWithObject:model];
        [RLMFeedEntity createOrUpdateInRealm:realm withValue:obj];
    }
    
    if (!inTransaction) {
        [realm commitWriteTransaction];
    }
}
- (NSArray *)getFeedModels {
    RLMResults *realmResults = [RLMFeedEntity allObjectsInRealm:[RLMRealm defaultRealm]];
    NSMutableArray *models = [NSMutableArray new];
    for (RLMFeedEntity *obj in realmResults) {
        [models addObject:[obj configureObject]];
    }
    
    return models;
}

@end
