//
//  RealmDataStore.h
//  TestNikitaSusoev
//
//  Created by Никита on 25.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RealmDataStore : NSObject

+ (RealmDataStore *)defaultStore;
+ (void)setupMigrationIfNeeded;

- (void)saveFeedModels:(NSArray *)models;
- (NSArray *)getFeedModels;

@end
