//
//  NSArray+Extras.h
//  TestNikitaSusoev
//
//  Created by Никита on 26.02.18.
//  Copyright © 2018 Nikita Susoev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Extras)

typedef NSInteger (^compareBlock)(id a, id b);

-(NSUInteger)indexForInsertingObject:(id)anObject sortedUsingBlock:(compareBlock)compare;

@end
